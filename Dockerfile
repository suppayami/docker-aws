FROM golang:1.9.1-alpine3.6 AS dependencies

RUN  apk --no-cache add git \
  && go get -u github.com/awslabs/amazon-ecr-credential-helper/ecr-login/cli/docker-credential-ecr-login

FROM docker:stable

COPY --from=dependencies /go/bin/docker-credential-ecr-login /bin/
RUN apk -Uuv add groff jq less python py-pip
RUN pip install awscli
RUN apk --purge -v del py-pip
RUN rm /var/cache/apk/*

CMD ["sh"]
